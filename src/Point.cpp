// Copyright 2021 Haute école d'ingénierie et d'architecture de Fribourg
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**************************
 * @file Point
 * @author Naël Telfser <nael.telfser@edu.hefr.ch>
 *
 * @brief //TODO:Include brief
 *
 * @date 05.04.22
 * @version 0.1.0
 *************************/

#include "Point.h"

Point::Point(const Point &point) : x(point.x), y(point.y), name(point.name) {}


Point::Point(double x, double y, std::string name) : x(x), y(y), name(name) {}


double Point::getX() const {
    return x;
}

double Point::getY() const {
    return y;
}

std::string Point::getName() const {
    return name;
}


void Point::setX(double x) {
    this->x = x;
}

void Point::setY(double y) {
    this->y = y;
}

void Point::setName(std::string name) {
    this->name = name;
}

double Point::distance(const Point &point) const {
    return sqrt(pow(point.x - x, 2) + pow(point.y - y, 2));
}


Point &Point::operator+=(const Point &other) {
    x += other.x;
    y += other.y;
    return *this;
}

Point &Point::operator-=(const Point &other) {
    x -= other.x;
    y -= other.y;
    return *this;
}

Point &Point::operator*=(const double other) {
    x *= other;
    y *= other;
    return *this;
}

Point &Point::operator/=(const double other) {
    if (other == 0) {
        throw std::invalid_argument("Division by zero");
    }
    x /= other;
    y /= other;
    return *this;
}

Point Point::operator+(const Point &other) {
    return Point(*this) += other;
}

Point Point::operator-(const Point &other) {
    return Point(*this) -= other;
}

Point Point::operator*(const double other) {
    return Point(*this) *= other;
}

Point Point::operator/(const double other) {
    if (other == 0) {
        throw std::invalid_argument("Division by zero");
    }
    return Point(*this) /= other;
}

bool Point::operator==(const Point &rhs) const {
    return x == rhs.x &&
           y == rhs.y &&
           name == rhs.name;
}

bool Point::operator!=(const Point &rhs) const {
    return !(rhs == *this);
}
