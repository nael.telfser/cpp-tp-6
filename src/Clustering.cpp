//
// Created by beatw on 7/14/2021.
//

#include "Clustering.h"

#include "TClosestPair.h"

#include <vector>

#include <random>
#include <numeric>
#include <algorithm>
#include <optional>
#include <iostream>

namespace Clustering{

    /**
     * Initialized cluster for KMeans algorithm
     *
     * Randomly selects one of the points to be the center of each cluster
     *
     * TODO: Implement pseudo code of
     * @param pts
     * @param nbOfClusters
     * @param seed
     * @return
     */
    std::vector<Cluster> initKMeans(const std::vector<Point> &pts, size_t nbOfClusters, int seed) {

        std::vector<Cluster> clusters;
        clusters.reserve(nbOfClusters);
        std::random_device rd;
        std::default_random_engine e1(rd());
        std::uniform_int_distribution<int> uniform_dist(0, pts.size() - 1);
        for(size_t i = 0; i < nbOfClusters; i++){
            int index = uniform_dist(e1);
            clusters.emplace_back(Cluster(pts[index]));
        }
        return clusters;
    }

    /**
     * Return the closest cluster for a given point.
     *
     * TODO: Use std::min_element to implement this function.
     *
     * @param clusters
     * @param p
     * @return
     */
    Cluster& closestCluster(std::vector<Cluster> &clusters, const Point &p) {
        Cluster &closest = *(std::min_element(clusters.begin(), clusters.end(), [&p](const Cluster &a, const Cluster &b) {
            return a.distance(p) < b.distance(p);
        }));
        return closest;
    }


    /**
     * Perform the KMeans clustering algorithm.
     *
     *
     * @param pts
     * @param nbOfClusters
     * @param seed
     * @return
     */
    std::vector<Cluster> kmeans(const std::vector<Point> &pts, size_t nbOfClusters, int seed){
        std::vector<Cluster> clusters = Clustering::initKMeans(pts, nbOfClusters, seed);

        //Assign points to clusters until the clusters are stable (do not move)
        bool stable = false;
        while(!stable){
            stable = true;
            for(size_t i = 0; i < pts.size(); i++){
                Cluster &closest = Clustering::closestCluster(clusters, pts[i]);
                closest.addPoint(pts[i]);
            }
            for(Cluster &c : clusters){
                if(c.recomputeCenter()){
                    stable = false;
                }
            }
            if (!stable) {
                for(Cluster &c : clusters){
                    c.clear();
                }
            }
        }

        return clusters;
    }

    /**
     * Converts all points into clusters
     * @param pts
     * @return
     */
    std::vector<Cluster> initHierchical(const std::vector<Point> &pts){

        std::vector<Cluster> clusters;
        for(const Point &p : pts){
            clusters.emplace_back(Cluster(p));
        }
        return clusters;
    }

    /**
     * Performs a hierarchical clustering with nbOfCluster number of clusters.
     *
     * TODO: Use ClosestPair to find the closest clusters to merge
     * @param pts
     * @param nbOfClusters
     * @return
     */
    std::vector<Cluster> hierarchical(const std::vector<Point> &pts, size_t nbOfClusters){
        std::vector<Cluster> clusters = initHierchical(pts);

        //merge two the closest clusters until there are only nbOfClusters left
        while(clusters.size() > nbOfClusters){
            ClosestPair<Cluster> cp;
            std::pair<Cluster*, Cluster*> closest = cp.closestPair(clusters);
            Cluster &c1 = *closest.first;
            Cluster &c2 = *closest.second;
            for(const Point &p : c2.getPoints()){
                c1.addPoint(p);
            }
            clusters.erase(std::remove(clusters.begin(), clusters.end(), *closest.second), clusters.end());
        }
        return clusters;
    }
}